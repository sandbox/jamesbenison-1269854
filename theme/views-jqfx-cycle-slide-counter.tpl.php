<?php

/**
 * @file
 * Views jQfx: Cycle slide counter template file.
 */
?>
<div<?php print $attributes; ?>>
  <span class="num">1</span> <?php print t('of'); ?> <span class="total">1</span>
</div>
