<?php

/**
 * @file
 * Views jQfx: Cycle pager field template file.
 */
?>
<div<?php print $attributes; ?>>
  <?php print $rendered_field_items; ?>
</div>
