<?php

/**
 * @file
 * Views jQfx: Cycle controls template file.
 */
?>
<div<?php print $attributes; ?>>
  <?php print $rendered_control_previous; ?>
  <?php print $rendered_control_pause; ?>
  <?php print $rendered_control_next; ?>
</div>
