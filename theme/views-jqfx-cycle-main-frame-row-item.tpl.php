<?php

/**
 * @file
 * Views jQfx: Cycle main frame row item template file.
 */
?>
<div<?php print $attributes; ?>>
  <?php print $item; ?>
</div>
