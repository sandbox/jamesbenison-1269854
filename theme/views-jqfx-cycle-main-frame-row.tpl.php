<?php

/**
 * @file
 * Views jQfx: Cycle main frame row template file.
 */
?>
<div<?php print $attributes; ?>>
  <?php print $rendered_items; ?>
</div>
